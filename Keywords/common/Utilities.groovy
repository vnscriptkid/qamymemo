package common
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Utilities {
	/**
	 * Login with username and password
	 */
	@Keyword
	def static login(String username, String password) {
		WebUI.setText(findTestObject('Page_Sign In/input_Username'), username)

		WebUI.setEncryptedText(findTestObject('Page_Sign In/input_Password'), password)

		WebUI.click(findTestObject('Page_Sign In/button_Sign in'))
	}

	/**
	 * Login with username and password
	 */
	@Keyword
	def static addRecepients(String emailToSent, String emailToCC) {
		WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_RECIPIENTS'))

		WebUI.setText(findTestObject('Page_myMemo - PETRONAS/Recepients Section/input_Search Username Sent-to'), emailToSent)

		WebUI.waitForPageLoad(2);

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/Recepients Section/span_First User To Send'))

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/Recepients Section/span_CC To'))

		WebUI.setText(findTestObject('Page_myMemo - PETRONAS/Recepients Section/input_Search Username CC-to'), emailToCC)

		WebUI.waitForPageLoad(2);

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/Recepients Section/span_First User To CC'))

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/Recepients Section/button_Confirm Recepients'))
	}
}