package memo
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import common.Utilities
import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SendInfoMemo {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@Given("I am a logged-in user")
	def I_am_a_logged_in_user() {
		WebUI.openBrowser('')

		WebUI.maximizeWindow()

		WebUI.navigateToUrl('http://qamymemo.petronas.com/')

		WebUI.click(findTestObject('Object Repository/Page_Home Realm Discovery/span_Petroliam Nasional Berhad'))

		Utilities.login('petronas\\son.nguyenbahoang', 'gVSc8o4YsVBga0t2lu29bw==')
	}

	@When('I create an memo with subject (.*) and content (.*)')
	def I_create_an_memo_with_subject_and_content(String subject, String content) {
		WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Create New Memo'))

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_For Your Information Memo'))

		WebUI.setText(findTestObject('Page_myMemo - PETRONAS/input_Memo Subject'), subject)

		Utilities.addRecepients('user.nextg1@petronas.com', 'user.nextg2@petronas.com')

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Confirm Create Memo'))

		WebUI.setText(findTestObject('Page_myMemo - PETRONAS/input_Memo Content'), content)

		WebUI.click(findTestObject('Object Repository/Page_myMemo - PETRONAS/button_Send'))

		WebUI.delay(1)

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Send Confirm'))
	}

	@Then("I should see the memo in sent list with above (.*) and (.*)")
	def I_should_see_the_memo_in_sent_list(String subject, String content) {
		WebUI.delay(3)

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Sent'))

		WebUI.delay(2)

		WebUI.click(findTestObject('Page_myMemo - PETRONAS/div_First memo item'))

		WebUI.verifyElementText(findTestObject('Page_myMemo - PETRONAS/Memo Details/span_Memo heading'), subject)

		WebUI.verifyElementText(findTestObject('Page_myMemo - PETRONAS/Memo Details/p_Memo content'), content)

		WebUI.closeBrowser()
	}
}