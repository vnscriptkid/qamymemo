#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@SendInfoMemo
Feature: Send Information Memo

	As a user, I want to create a info memo, then send to others

  @Valid
  Scenario Outline: User can send an information memo to one user and cc to another
    Given I am a logged-in user
    When I create an memo with subject <subject> and content <content>
    Then I should see the memo in sent list with above <subject> and <content>
    
    Examples: 
      | subject | content           |
      | An information memo | Memo content |
      | Another memo | Another Memo content |