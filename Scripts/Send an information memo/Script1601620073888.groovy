import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import common.Utilities as Utilities
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://qamymemo.petronas.com/')

WebUI.click(findTestObject('Object Repository/Page_Home Realm Discovery/span_Petroliam Nasional Berhad'))

CustomKeywords.'common.Utilities.login'('petronas\\son.nguyenbahoang', 'gVSc8o4YsVBga0t2lu29bw==')

WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Create New Memo'))

WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_For Your Information Memo'))

WebUI.setText(findTestObject('Page_myMemo - PETRONAS/input_Memo Subject'), 'New Automation Test - Send an information memo')

CustomKeywords.'common.Utilities.addRecepients'('user.nextg1@petronas.com', 'user.nextg2@petronas.com')

WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Confirm Create Memo'))

WebUI.setText(findTestObject('Page_myMemo - PETRONAS/input_Memo Content'), 'Memo Content')

WebUI.click(findTestObject('Object Repository/Page_myMemo - PETRONAS/button_Send'))

WebUI.waitForPageLoad(1)

WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Send Confirm'))

WebUI.waitForPageLoad(3)

WebUI.click(findTestObject('Page_myMemo - PETRONAS/button_Sent'))

WebUI.waitForPageLoad(2)

WebUI.click(findTestObject('Page_myMemo - PETRONAS/div_First memo item'))

WebUI.verifyElementText(findTestObject('Page_myMemo - PETRONAS/Memo Details/span_Memo heading'), 'New Automation Test - Send an information memo')

WebUI.verifyElementText(findTestObject('Page_myMemo - PETRONAS/Memo Details/p_Memo content'), 'Memo Content')

WebUI.closeBrowser()

