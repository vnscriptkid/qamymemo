<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Memo heading</name>
   <tag></tag>
   <elementGuidId>96cc7c8e-9a1a-4d33-80aa-4fb126a8c119</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#splitTwo .my-memo-detail-inbox-title .detail-inbox-title-middle div.ditm-memo-subject span.ditmms-content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='splitTwo']/div/memo-detail/div/div/draft-detail/div/div/div/div[2]/div/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>my-draft-content-subject-title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Automation Test - Send an information memo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;splitTwo&quot;)/div[@class=&quot;dashboard-memo-detail&quot;]/memo-detail[1]/div[@class=&quot;my-memo-detail&quot;]/div[@class=&quot;general-memo&quot;]/draft-detail[1]/div[@class=&quot;my-memo-draft-detail&quot;]/div[@class=&quot;my-draft-content-container&quot;]/div[@class=&quot;my-draft-content-header&quot;]/div[@class=&quot;my-draft-content-header-bottom&quot;]/div[@class=&quot;mdchb-left&quot;]/span[@class=&quot;my-draft-content-subject-title&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='splitTwo']/div/memo-detail/div/div/draft-detail/div/div/div/div[2]/div/span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subject'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Serial No'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Type:'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[2]/div/span[2]</value>
   </webElementXpaths>
</WebElementEntity>
